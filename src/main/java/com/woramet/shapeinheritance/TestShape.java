/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeinheritance;

/**
 *
 * @author User
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape (1 , 2);
        shape.print();
        System.out.println("---------------------");
        
        Circle circle1 = new Circle (3);
        circle1.print();
        System.out.println("---------------------");
        
        Circle circle2 = new Circle (4);
        circle2.print();
        System.out.println("---------------------");
        
        Triangle triangle = new Triangle (4 , 3);
        triangle.print();
        System.out.println("---------------------");
        
        Rectangle rectangle = new Rectangle(4 , 3);
        rectangle.print();
        System.out.println("---------------------");
        
        Square square = new Square(2);
        square.print();
        System.out.println("---------------------");
        
        System.out.println("Circle1 is Shape: " + (circle1 instanceof Shape));
        System.out.println("Circle2 is Shape: " + (circle2 instanceof Shape));
        System.out.println("Triangle is Shape: " + (triangle instanceof Shape));
        System.out.println("Rectangle is Shape: " + (rectangle instanceof Shape));
        System.out.println("Square is Shape: " + (square instanceof Shape));
        System.out.println("Square is Rectangle: " + (square instanceof Rectangle));
        System.out.println("Rectangle is Square: " + (rectangle instanceof Square));
        System.out.println("Shape is Triangle: " + (shape instanceof Triangle));
        System.out.println("---------------------");
        
        Shape[] shapes = {triangle , circle1 , circle2 , rectangle , square};
        for (int i = 0; i < shapes.length; i++){
            shapes[i].print();  
        }
    }
}
