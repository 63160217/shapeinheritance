/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeinheritance;

/**
 *
 * @author User
 */
public class Square extends Rectangle {
    private double side ;
     public Square(int side) {
        super(side , side);
        System.out.println("Square created");
        this.side = side;
    }
    
    @Override
    public void print(){
        System.out.println("Square print");
        System.out.println("Square: side: " + side + " area = " + calArea());
    }
}
