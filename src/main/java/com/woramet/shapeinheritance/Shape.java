/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeinheritance;

/**
 *
 * @author User
 */
public class Shape {
    protected double x = 0;
        protected double y = 0;
    public Shape (double x , double y){
        System.out.println("Shape created");
        this.x = x;
        this.y = y;
    }
    
    public double calArea(){
        return x * y; 
    }
    
    public void print(){
        System.out.println("Shape print");
        System.out.println("Shape: x: " + x + " y: " + y + " area = " + calArea());
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    
}
