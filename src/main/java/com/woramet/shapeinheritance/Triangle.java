/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeinheritance;

/**
 *
 * @author User
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    public Triangle(double base, double height) {
        super(base, height);
        System.out.println("Triangle created");
        this.base = base;
        this.height = height;
    }

    @Override
    public double calArea() {
        return 1/2.0 * base * height;
    }
    
    @Override
    public void print(){
        System.out.println("Triangle print");
        System.out.println("Triangle: base: " + base + " height: " + height + " area = " + calArea());
    }
}
